## 09.04.19
Continue Docker Getting Started tutorial from <a href="https://docs.docker.com/get-started/part2/" target="_blank">Docker Docs</a>.
### Share built Docker image to cloud for remote pulling
 - Create Docker Hub account
 - Create new <a href="https://hub.docker.com/r/yenthusiastic/get-started" target="_blank">Docker repo</a> to push the built container image
 - Added collaborator to repo
 - In terminal, log in to this account using `docker login` command
 - Upon successful login, tag the built image using `docker tag <image> <username>/<repository>:<tag>` command, specifying respective created image name, username and repository name
 - Publish the image using command 
```powershell
docker push <username>/<repository>:<tag>
```
In this note, the app is published to <a href="https://hub.docker.com/r/yenthusiastic/get-started/tags" target="_blank">yenthusiastic/get-started:friendlyhello</a>.
 - On another PC (already installed with Docker CE), pull and run the image from the remote Docker repo using command
```powershell
docker run -p 127.0.0.1:4000:80 <username>/<repository>:<tag>
``` 
 This command will automatically pull the image along with Python and all dependencies if the image is not already available on the local machine, and run the app after pulling.
The app has successfully been deployed on a PC running Ubuntu 16.10 with Docker installed and is accessible at <a href="http://127.0.0.1:4000">http://127.0.0.1:4000</a>.

`IMPORTANT NOTE:` If we dont't provide the local host IP address (*127.0.0.1*) at the `port` flag in the above command, i.e. only ` -p 4000:80` instead, the app will be accessible at <a href="http://0.0.0.0:4000">http://0.0.0.0:4000</a>. This means other computers on the same network will be able to reach it. 

### Run micro services
 - Install latest release of <a href="https://github.com/docker/compose/releases" target="_blank">Docker Compose</a> (1.24.0 as of time of writing):
```
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```
 - Set file permissions
```
sudo chmod +x /usr/local/bin/docker-compose
```
 - Create a `docker-compose.yml` YAML file (human-readable markup language) to define how Docker containers should behave in production. Example file from the tutorial with corresponding explanations of the commands:
```powershell
version: "3"
services:
  web:
    # replace username/repo:tag with your name and image details
    image: username/repo:tag
    deploy:
      replicas: 5    # Run 5 instances of that image as a service called 'web'
      resources:
        limits:
          cpus: "0.1"
          memory: 50M     # Limit each instance to use, at most, 10% of a single core of CPU time and 50MB of RAM.
      restart_policy:
        condition: on-failure   # Immediately restart containers if one fails.
    ports:
      - "4000:80"     # Map port 4000 on the host to port 80 of 'web'.
    networks:
      - webnet    # Instruct web’s containers to share port 80 via a load-balanced network called 'webnet'. (Internally, the containers themselves publish to web’s port 80 at an ephemeral port.)
networks:
  webnet:      # Define the 'webnet' network with the default settings (which is a load-balanced overlay network).
```
 -  Initialize the swarm with command `docker swarm init` (compulsory)
 - <a name="deploy_cmd">Run the load-balanced app with command</a> 
```powershell
docker stack deploy -c <docker-compose-file> <app-name>
```
In this case, the docker compose file is `docker-compose.yml`.
 This command causes our single service stack to run 5 container instances of our deployed image on one host. To check the ID of the service stack run
 ```powershell
 docker service ls
 ```
 - The service is automatically named as `<app-name_web>`. A single container running inside the service is called a task. To list the tasks of an app named, e.g. `friendlyapp`, run
```poweshell
 docker service ps friendlyapp_web
```
 - To **upscale the app**, simply change the value for `replicas` to a number > 5 in `docker-compose.yml` file, save the change and rerun the [deploy command](#deploy_cmd).
 - To take down the app, run `docker stack rm friendlyapp`.
 - To take down the swarm, run `docker swarm leave --force`.

