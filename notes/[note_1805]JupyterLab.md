## Virtual environment (venv) for JupyterLab
The venv enables a defined default environment and kernel for JupyterLab. 

Make and activate the venv named *jupyterlab*, using Python 3.7.
```bash
python3.7 -m venv jupyterlab
source jupyterlab/bin/activate
```
Exit the venv with `logout`


## Install
```bash
pip install jupyterlab
```

### Make IPyKernel
Creating an IPython kernels makes a venv accessible for JupyterLab. 
The kernel can be chosen in the drop-down menu (top right) in a notebook.

Make kernel **pyOTA** with displayed name (drop-down menu) **3.6_pyOTA**
```bash
python -m ipykernel install --user --name pyOTA --display-name 3.6_pyOTA
```
### Start JupyterLab
Go to the directory, that should be the default directory for JupyterLab.
If you want to have access to files above that directory, you need to create 
a symlink. **dirAbove** is the link name in the Jupyter directory. 

`ln -s ../../directoryAbove dirAbove`

To give anyone all access to a directory:

`sudo chmod -R a+rwx /path/to/folder`

As Jupyter only has the access rights of the user which executes it.
Python libraries like *werkzeug* don'T like the *app.py* file to be free for all.
For this file chane the permission:

`chmod -R 755 app.py`

Run JupyterLab public on port 8080:
```bash
jupyter lab --ip 0.0.0.0 --port 8080
```

### Unlimited runtime
Open a screen session named **jupyter** and start JupyterLab. 
Make sure you are in the venv.
```bash
screen -S jupyter
jupyter lab --no-browser --ip 0.0.0.0 --port 8080 
```
Exit with `Ctrl + A, D`

Reconnect with `screen -r jupyter`

## Sources
[Kernel Install](https://ipython.readthedocs.io/en/latest/install/kernel_install.html) 

[JupyterHub](https://jupyterlab.readthedocs.io/en/stable/user/jupyterhub.html)