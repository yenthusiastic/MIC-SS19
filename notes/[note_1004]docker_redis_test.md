## 10.04.19
### First try to link python app and redis store
 - Some useful new commands:
```powershell
docker container rm <CONTAINER ID>  # remove a container by ID
docker image rm  <IMAGE ID>   # remove an image by ID
docker system prune  # WARNING! This will remove:
                     #  - all stopped containers
                     #  - all networks not used by at least one container
                     #  - all dangling images
                     #  - all build cache
```

 - Create a new `docker-compose` file in the same folder to add links to redis 
```powershell
version: '3'  
services:  
  app:
    build: ./
    depends_on:
      - redis
    environment:
      - REDIS_HOST=redis
    ports:
      - "3000:80"
    links:
      - redis
  redis:
    image: redis
    expose:
      - 6379
    volumes:
      - redis_data:/data
volumes:  
  redis_data:
```

 - Run `docker-compose up -d`. This command builds, (re)creates, starts, and attaches to containers for a service and keeps it running in the background. Unless they are already running, this command also starts any linked services. 
 This commands creates 2 new containers called *test_app_1* and *test_redis_1* by default. The app runs on port 3000 at <a href="http://www.datalog.live:3000/" target="_blank">http://www.datalog.live:3000/</a>

### To link app container with already existing redis container
 - Start a new container running redis
```powershell
docker run -d -p 6379:6379 --name redis1 redis
```
 - Stop the app already running on port 3000 using its ID
 - In the folder containing `Dockerfile`, `docker-compose.yml`, `app.py` and `requirements.txt`, create build image for the app with a tag name
```
docker build --tag=testredis .
```
 - Create a new container from the image, give it a name and link it with the created redis container from above by its name
```powershell
docker run -d -p 3000:80 --link redis1:redis --name testredis1 testredis
```
We link it to the redis1 container (which is still running), and it will be referred to from within this container simply as redis.

### Extras
 - Run the Redis CLI in the container
```powershell
docker exec -it redis1 sh
```
 - Try out some basic Redis commands. If we send a "ping", should get "PONG" back:
```powershell
127.0.0.1:6379> ping
PONG
```
 - Try out some more commands (set a key and increment a counter)
```powershell
127.0.0.1:6379> set name mark
OK
127.0.0.1:6379> get name
"mark"
127.0.0.1:6379> incr counter
(integer) 1
127.0.0.1:6379> incr counter
(integer) 2
127.0.0.1:6379> get counter
"2"
```
 - And when we're done exit out of redis-cli and sh:
```powershell
127.0.0.1:6379> exit
# exit
```