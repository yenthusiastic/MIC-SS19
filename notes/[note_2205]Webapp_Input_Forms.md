## Date 22.05.2019
### Web application UI
- Default visualization: Map with markers showing all nodes in current viewport
- When hovering upon markers, show location
- When clicking on markers, show dat of node X at time T : {sensor ID, sensor description, sensor value} as table
- When moving viewport with the "Pan" tool, load all sensors in corresponding region
- Different data visualization possibilities
    -  Map
    -  Table
    -  CSV rows
- Alternative visualization: line chart for temperature/ humidity. Upon hovering should show {node ID, sensor ID, temperature, humidity} .Possible filter options:
    -  By node ID
    -  By sensor ID
    -  By time slider
 
Inspiration and Tutorials:
https://www.padmapper.com/
https://dash.plot.ly/interactive-graphing

### Notes:
- On loading site, query for Node ID, GPS sensor ID, Location, Timestamp
- On clicking on a marker, query for additional sensor ID, sensor type, sensor value at timestamp T of node X