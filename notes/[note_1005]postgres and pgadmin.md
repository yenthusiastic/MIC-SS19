## Make docker-compose config file

`docker-compose.yml`
```yml
version: '3'
services:
  postgres:
    image: postgres
    hostname: postgres
    ports:
      - "6543:5432"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: datalog_db
    volumes:
      - postgres-data:/var/lib/postgresql/data
    restart: unless-stopped

  pgadmin:
    image: dpage/pgadmin4
    depends_on:
      - postgres
    ports:
      - "5555:80"
    environment:
      PGADMIN_DEFAULT_EMAIL: pgadmin4@pgadmin.org
      PGADMIN_DEFAULT_PASSWORD: admin
    restart: unless-stopped

volumes:
  postgres-data:
```

## Build image and run container
Build image:
```bash
sudo docker-compose --build
```
Run container in detached mode (`-d`)
```bash
docker-compose up -d
```

## pgAdmin
Connect to pgAdmin at `http://127.0.0.1:5555`

Use credentials as defined in `docker-compose.yml`: 

**user:** pgadmin4@pgadmin.org 

**pwd:** admin

Connect to Postgres server by clicking on *Add New Server*:

<img src = "../media/pgadmin_make_connection1.png" width="720px" height="640px">

Under *Connection* tab, enter credentials as defined in `docker-compose.yml`: 

**user:** postgres 

**pwd:** postgres

## Connect to Postgres DB from bash
Get containerID of running postgres DB container
```bash
docker container ls   # get container ID here
```
Go inside the container
```bash
docker exec -it container-ID-here bash
```
Connect to DB using parameters as specified above in `docker-compose.yml` file
```bash
psql -U postgres -d datalog_db
```
To exit from current database, enter `\q`.

To exit from `psql`, enter `exit`.

## Check where the `postgres-data` volume is mounted in local host:
```bash
sudo docker volume ls   # find the id of the volume here
sudo docker volume inspect <volume-id-here> 
```

## Database management
- Create database named `datalog_db`
- Change timezone of DB
```
ALTER DATABASE postgres SET timezone TO 'Europe/Berlin';
```
Restart DB/ PgAdmin for timezone change to take effect.
- Under `datalog_db`, created 4 tables
  -  NODES
  -  SENSORS
  -  GPS_DATA
  -  TEMPC_HUMID_DATA
  -  TEMPF_HUMID_DATA

The data model is as follows:
<img src = "../media/data_model.png" width="720px">

SQL query to create the tables:
```bash
CREATE TABLE public."NODES"
(
    "nodeID" integer NOT NULL,
    "nodeDescription" text NOT NULL,
    "sensorID" integer[],
    PRIMARY KEY ("nodeID")
);
CREATE TABLE public."SENSORS"
(
    "sensorID" integer NOT NULL,
    "sensorType" text NOT NULL,
    "dataType" text NOT NULL,
    "sensorDescription" text NOT NULL,
    PRIMARY KEY ("sensorID")
);
CREATE TABLE public."TEMPC_HUMID_DATA"
(
    "index" bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    "temperature" real NOT NULL,
    "humidity" real NOT NULL,
    PRIMARY KEY ("index")
);
CREATE TABLE public."TEMPF_HUMID_DATA"
(
    "index" bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    "temperature" real NOT NULL,
    "humidity" real NOT NULL,
    PRIMARY KEY ("index")
);
CREATE TABLE public."GPS_DATA"
(
    "index" bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    "latitude" real NOT NULL,
    "longitude" real NOT NULL,
    PRIMARY KEY ("index")
);
```

The structure of the data object sent by nodes to DB is as follows:
```
{
    msg: [
        {
            sensorID: 1;
            data: (00000, 11111);
        },
        {
            sensorID: 2;
            data: [27.3, 50.6]; 
        }
    ]
}
```
- Insert dummy data entry to database, for example, to table `GPS_DATA_NEW`
```bash
INSERT INTO "GPS_DATA_NEW" VALUES (DEFAULT, 1, current_timestamp, 51.5005622, 6.543568)
```
> NOTE: the order of the parameters in SQL query have to be kept the same as above, where: 
>
>  - DEFAULT will be parsed by Postgres as automatically incremented index
>
>  - 1 is sensor ID
>
>  - current_timestamp will be parsed by Postgres as current timestamp
>
>  - 51.5005622 is latitude
>
>  - 6.543568 is longtitude