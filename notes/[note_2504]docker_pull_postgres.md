## 25.04.2019
### Installing Postgres from docker image
Notes following tutorial from [Hackernoon](https://hackernoon.com/dont-install-postgres-docker-pull-postgres-bee20e200198).
- Pull Postgres docker image with `docker pull postgres`
- Create a volume for persistent storage beyond the container’s lifecycle with `mkdir $HOME/docker/volumes/postgres`
- Start the Postgres container using <a name="dockerrun">docker run</a> command, provide appropriate flags
```powershell
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 127.0.0.1:5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres
```
Explanation of the flags used in above command:
> `--rm`: Automatically remove the container and its associated file system within the container upon exit to release disk space.
>
> `--name`: A name for the container. Note that two existing (even if they are stopped) containers cannot have the same name. To re-use a name, either pass `--rm` flag to the docker run command or explicitly remove the container by using the command `docker rm [container name]`
>
> `--e`: Expose environment variable named *POSTGRES_PASSWORD* with value *docker* to the container. This environment variable sets the superuser password for PostgreSQL. There are additional environment variables you can set like *POSTGRES_USER* and *POSTGRES_DB*. *POSTGRES_USER* sets the superuser name. If not provided, the superuser name defaults to *postgres*. *POSTGRES_DB* sets the name of the default database to setup. If not provided, it defaults to the value of *POSTGRES_USER*.
>
> `--d`: Launches the container in detached mode (in the background).
>
> `--p`: Bind port *5432* on localhost (*127.0.0.1*) to port *5432* within the container. This option enables applications running out side of the container to be able to connect to the Postgres server running inside the container. If localhost IP address *127.0.0.1* is not provided, it will run on *0.0.0.0* which means other computers on the same network will be able to reach it.
>
> `--v`: Mount $*HOME/docker/volumes/postgres* on the host machine to the container-side volume path */var/lib/postgresql/data* created inside the container. This ensures that postgres data persists even after the container is removed.

- Install the Postgres interactive terminal PSQL (for Ubuntu 14.04, 16.04, 18.04, postgres-client-10 is available). For other OS, check [this](https://www.postgresql.org/download/).
```powershell
sudo apt-get install postgres-client-10
```
- Connect to the container from another application using PSQL
```powershell
psql -h localhost -U postgres -d postgres
```
Note: 
> `-h`: hostname
>
> `-U`: username
>
> `-d`: database name

For more flags info check [this](https://www.postgresql.org/docs/current/app-psql.html).
- Enter *docker* when prompted for password, or whatever password was set in the `docker run` command [above](#dockerrun).
- Create an example table named *test_sensor* for the sensor database
```
postgres=# CREATE TABLE test_sensor(
postgres(# id integer,
postgres(# sensor_id integer,
postgres(# sensor_desc text,
postgres(# wifi_ssid text,
postgres(# gps_loc point,
postgres(# humidity real,
postgres(# temperature real);
```
More details on data types and SQL commands, check the [documentation](https://www.postgresql.org/docs/10/index.html).
- Show the table structure of table *test_sensor*
```
postgres=# \d test_sensor
```
Result:
Table "public.test_sensor"

   Column    |  Type   | Collation | Nullable | Default
-------------|---------|-----------|----------|---------
 id          | integer |           |          |
 sensor_id   | integer |           |          |
 sensor_desc | text    |           |          |
 wifi_ssid   | text    |           |          |
 gps_loc     | point   |           |          |
 humidity    | real    |           |          |
 temperature | real    |           |          |

- Try to insert values into table
```powershell
INSERT INTO test_sensor VALUES (1, 1, 'test', '12345', '(51.500756, 6.545757)', 0.98765, 27.0);
```
- Check the inserted entry
```powershell 
select * from test_sensor;
```
Result:

 id | sensor_id | sensor_desc | wifi_ssid |       gps_loc        | humidity | temperature
----|-----------|-------------|-----------|----------------------|----------|-------------
  1 |         1 | test        | 12345     | (51.500756,6.545757) |  0.98765 |          27

To quit, use command `\q`

