# Server

**Domain:** 
- datalog.live

**IP:** 

    - 116.203.16.176
    - 2a01:4f8:c2c:5043::/64

[Hetzner FAQ](https://wiki.hetzner.de/index.php/CloudServer/en)






## Basic Setup Changes

### Keymapping (Keyboard Layout)
In the Hetzner online terminal:

`sudo dpkg-reconfigure keyboard-configuration`

log out or restart server for changes to take effect

### Make bash the default shell

```
ls -l `which sh`
```
/bin/sh -> dash
```
sudo dpkg-reconfigure dash
``` 
Select "no" when you're asked

```
ls -l `which sh` 
```
/bin/sh -> bash

### Add Users
In the Hetzner online terminal:
 - add user testuser
 - add user testuser to sudoers group 
 - make sure bash is assigned as default shell
```bash
adduser testuser
usermod -aG sudo testuser
sudo usermod -s /bin/bash testuser
```

Change into user testuser:
`su testuser`

Check that you are infact the user:
`whoami`


### Make SSH keypair
On your local linux machine: 
 - create keypair *dev-server*
 - copy the public key to your server
```bash
ssh-keygen -t ed25519 -C "dev-server"
ssh-copy-id -i ~/.ssh/dev-server.pub testuser@SERVER-IP
```

### SSH: deactivate root & password login, change port
Only keyfile based login will be allowed.
Make sure you copied the ssh keyfile to all users, before you continue here!

On the server:
 - edit the sshd config file:
`nano /etc/ssh/sshd_config`

Change *#Port 22* to:
`Port 25`

Change *PermitRootLogin yes* to:
`PermitRootLogin no`

Change *#PubkeyAuthentication no* to:
`PubkeyAuthentication yes`

Change *#PasswordAuthentication yes* to:
`PasswordAuthentication no`

Check the bottom of the file for any overwriting setting of the password authentification and change them accordingly.

Exit nano and restart the sshd daemon:
`systemctl restart sshd.service`

## Get SSL Certificate
Make a wildcard certificate so all subdomains are valid. 

Domain: datalog.live

Wildcard: *.datalog.live

```bash
sudo add-apt-repository ppa:certbot/certbot
sudo apt update
sudo apt install certbot
sudo certbot certonly --manual -d *.datalog.live -d datalog.live --agree-tos --no-bootstrap --manual-public-ip-logging-ok --preferred-challenges dns-01
```
After the last command, the following message should appear:
```console
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please deploy a DNS TXT record under the name
_acme-challenge.datalog.live with the following value:

NHddk2y7x3Sy-N1Cq2LlbHBU8tjQJm58RSsCaaEHcNY

Before continuing, verify the record is deployed.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue

```
Create a DNS TXT record with the following values to the domain's settings: 
> **Note:** Don't insert the domain name in the ***Host** textfield.

```shell
Host: _acme-challenge
Value: NHddk2y7x3Sy-N1Cq2LlbHBU8tjQJm58RSsCaaEHcNY
```

Source: [Install Let’s Encrypt Free SSL Wildcard Certificate on Ubuntu 18](https://medium.com/@lakin.mohapatra/generate-lets-encrypt-free-wildcard-certificate-on-ubuntu-18-dcf26f458e13)



# Applications

## Install openresty

```bash
wget -qO - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
sudo apt-get -y install software-properties-common
sudo add-apt-repository -y "deb http://openresty.org/package/ubuntu $(lsb_release -sc) main"
sudo apt-get update
sudo apt-get install openresty
```


## Install Let's Encrypt certificate
```bash
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install certbot python-certbot-nginx 
sudo certbot --nginx
sudo crontab -e
30 03 * * 6 /etc/letsencrypt/renew-cron.sh
```

## Basic nginx.conf
Find it at:
`/usr/local/openresty/nginx/conf/nginx.conf`

```conf
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       80 default_server;
        server_name  localhost;
        return 301 https://dev.iota.pw/$request_uri;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

#        location / {
#            root   html;
#            index  index.html index.htm;
#        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
#        error_page   500 502 503 504  /50x.html;
#        location = /50x.html {
#            root   html;
#        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }
    
        server {
        listen       443 ssl http2;
        server_name  localhost;

        ssl_certificate      /etc/letsencrypt/live/datalog.live/fullchain.pem;
        ssl_certificate_key  /etc/letsencrypt/live/datalog.live/privkey.pem;

        ssl_session_cache    shared:SSL:1m;
        ssl_session_timeout  5m;

        ssl_ciphers  HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers  on;

        location / {
            root   html;
            index  index.html index.htm;
        }
    }

    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


}
```


## Install NodeJS and NPM
```bash
sudo apt install nodejs
sudo apt install npm
```

## Install JupyterLab
[Install JupyterLab](notes/[note_1805]JupyterLab.md)


### Docker
[Install Docker](notes/[note_0804]install_docker_test.md)

[Install Docker Compose](https://github.com/docker/compose/releases)

## PostgresQL
### Install postgres client
```bash
sudo apt-get install postgresql-client-10
```


