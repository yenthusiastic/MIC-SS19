## Useful SQL queries to use later

- Assuming user selects a certain sensor ID from dropdown menu, first have to check the sensor type of the sensor
```bash
select "dataType" from "SENSORS" where "sensorID"=2
```
- If the type is GPS_DATA, then select the GPS latitude and longitude of the sensor at the most current timestamp from the GPS_DATA table
```bash
SELECT "latitude", "longitude" FROM public."GPS_DATA_NEW" WHERE "sensorID" = 2 ORDER BY "timestamp" DESC LIMIT 1  
```
- If the type is TEMPC_HUMID_DATA, then select the temperature and humidity of the sensor at the most current timestamp from the TEMPC_HUMID_DATA table
```bash
select "temperature", "humidity" from "TEMPC_HUMID_DATA" WHERE "sensorID" = 4 ORDER BY "timestamp" DESC LIMIT 1  
```
- Display all possible sensor types
```bash
select distinct "sensorType" from "SENSORS" 
```
- Display all sensor IDs of a certain type
```bash
select "sensorID" from "SENSORS" where "sensorType" = 'temperature-humidity'
select "sensorID" from "SENSORS" where "sensorType" = 'GPS'
```
- Get all GPS data by unique sensor IDs and of most recent timestamp
```bash
SELECT DISTINCT ON ("sensorID") "sensorID", "timestamp", "latitude", "longitude" FROM public."GPS_DATA_NEW" ORDER BY "sensorID" DESC, "timestamp" DESC
```
- Get nodeID of sensor by sensorID
```bash
SELECT "nodeID" FROM public."NODES" WHERE '1'=ANY("sensorID")
```
- Get all GPS data with node ID
```bash
SELECT DISTINCT ON ("GPS_DATA_NEW"."sensorID") "GPS_DATA_NEW"."sensorID", "timestamp", "latitude", "longitude", "nodeID" FROM public."GPS_DATA_NEW" INNER JOIN public."NODES" ON ("GPS_DATA_NEW"."sensorID" = ANY("NODES"."sensorID")) 
```

- Create VIEW "GPS_last_location"
```bash
SELECT DISTINCT ON ("NODES"."nodeID") 
	"GPS_DATA_1"."sensorID", 
	"GPS_DATA_1"."timestamp",
    "GPS_DATA_1".latitude,
    "GPS_DATA_1".longitude,
    "NODES"."nodeID"
FROM 
(( SELECT "GPS_DATA".index,
            "GPS_DATA"."sensorID",
            "GPS_DATA"."timestamp",
            "GPS_DATA".latitude,
            "GPS_DATA".longitude
           FROM "GPS_DATA" 
          ORDER BY "GPS_DATA"."timestamp" DESC) AS "GPS_DATA_1"
     JOIN "NODES" ON "GPS_DATA_1"."sensorID" = ANY ("NODES"."sensorID"));;
```

- Create VIEW "GPS_last_loc_desc"
```bash
SELECT "GPS_last_location"."nodeID",
    "SENSORS"."sensorID",
    "SENSORS"."sensorDescription",
    "GPS_last_location"."timestamp",
    "GPS_last_location".latitude,
    "GPS_last_location".longitude
   FROM "GPS_last_location"
     JOIN "SENSORS" ON "SENSORS"."sensorID" = "GPS_last_location"."sensorID";
```