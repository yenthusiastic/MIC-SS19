# Node hardware
## ESP32 with GPS sensor and DHT22 Temperature + Humidity sensor
![hardware](../media/node_hardware_full.jpg)

<p>

## Displaying sensor data on OLED screen:
![hardware](../media/node_hardware_screen.jpg)

<p>

## Wiring scheme
(to be updated)