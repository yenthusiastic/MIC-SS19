## Installed Python versions
List all installed Python versions:
```bash
apt list --installed | grep python
```
Check currently used python versions:
```bash
python -V
python3 -V
pip -V
pip3 -V
```


## Upgrade Python
```bash
sudo apt update
sudo apt install python-apt
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.7 python3.7-venv python3.7-dev
type python
python --version
```
The last command should return *Python 3.7.x*.
> Exchange python3.7 for *python3.6* if you need to install it. 
> For example in a virtual environment.


#### Remove Python 2.x
If Python 2 is still installed remove it.
```bash
sudo apt purge python2.7-minimal
sudo apt purge python2.7
```
#### Make Python 3.7 default
Set Python3.7 as default for **python**:
```bash
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
sudo update-alternatives  --set python /usr/bin/python3.7
```

Set Python3.7 as default for **python3**:
```bash
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
sudo update-alternatives  --set python3 /usr/bin/python3.7
```

Errors with packages *gi* and *apt_pkg*:

Check '/usr/lib/python3/dist-packages': [Source](https://askubuntu.com/questions/480908/problem-with-update-manager-no-module-named-apt-pkg-in-ubuntu-13-10-having-i/1063420#1063420)



## PIP (Python Package Index)

#### Re-/Install pip
```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --force-reinstall
python3 get-pip.py --force-reinstall
pip -V
pip3 -V
```

#### Upgrade pip
```bash
pip install --upgrade pip
```
Run `pip --version` to check if it is successfully upgraded. 

#### Fix pip errors
If `pip -V` shows the error `pip ImportError: cannot import name 'main'`:
```bash
which pip #/usr/bin/pip
sudo nano /usr/bin/pip
```
In the file, fix following lines as:
```
from pip import __main__
if __name__ == '__main__':
     sys.exit(__main__._main())
```

Fix pip error message about missing locale (can also fix unrelated issues):
```bash
sudo locale-gen en_US.UTF-8
sudo locale-gen de_DE.UTF-8
sudo update-locale LANG=en_US.UTF-8
```

## Virtual environment
Go into the project directory and make the virtual environment (here: *datalog*). 
Specify the exact Python version you want to use. 
Activate the virtual environment.

```bash
python3.7 -m venv datalog
source datalog/bin/activate
```
> Exit the venv with `deactivate`

## Sources
[How to Install Python 3.7 on Ubuntu 18.04](https://linuxize.com/post/how-to-install-python-3-7-on-ubuntu-18-04/) 

[Install Python3 on Ubuntu 18.04 and Set Up a Virtual Programming Environment](https://vitux.com/install-python3-on-ubuntu-and-set-up-a-virtual-programming-environment/) 

[How to Install Pip on Ubuntu 18.04](https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/)