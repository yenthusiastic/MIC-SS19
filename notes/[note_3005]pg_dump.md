### Export schema of Postgres database using pg_dump
#### Install postgres
```bash
sudo apt-get install postgres
```
#### Dump postgres data to file:
  -  SYNTAX:
```bash
pg_dump -d <databasename> -h <hostname> -p <port> -U <username> -n <schemaname> -f <location of the dump file>
```
 - Example:
```
pg_dump -d datalog_db -h localhost -p 6543 -U postgres -n public -f datalog_db.dmp
```
Received error
```
pg_dump: server version: 11.2 (Debian 11.2-1.pgdg90+1); pg_dump version: 10.7 (Ubuntu 10.7-0ubuntu0.18.04.1)
pg_dump: aborting because of server version mismatch
```
This means that the postgres version of the container is the latest version (11.2) but the postgres version of the local host (VPS, Ubuntu 18.04) is only 10.7. Try to change the version of postgres in `docker-compose.yml` file to 10 by:
```
 ...
    postgres:
        image: postgres:10
```
And run `docker-compose up --build` to build the image again but this time receives error
```
postgres_1  | 2019-05-30 13:22:25.316 UTC [1] DETAIL:  The data directory was in itialized by PostgreSQL version 11, which is not compatible with this version 10 .8 (Debian 10.8-1.pgdg90+1).
```
This is because the persisted data which was created before by version 11.2 is not compatible with version 10.7. Try to change the name of the persisted data volume in `docker-compose.yml` file
```
...
    volumes:
      - postgres-data-10:/var/lib/postgresql/data
      
      ...

volumes:
  postgres-data-10:
```
The same error as above still shows up. Try to remove the docker volume associated with the old postgres:latest container
```bash
docker volume ls   #find volume names of all persisted volumes here
docker volume prune   #remove all unused volumes
docker volume rm --force <volume-name-here>  #force remove volume by name, for this application the volume name should be webapp_dbaccess_postgres-data
```
If an error shows up that the volume is in use, perform a docker system prune and try again:
```bash
docker system prune
docker volume rm webapp_dbaccess_postgres-data
```
Now run `docker-compose up --build` again, the problem with version mismatch has been solved. 

Create the schema and tbales inside the database anew and try `pg_dump` again. This time the database has been dumped successfully. Example content of `datalog_db.dump` file:
```
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8 (Debian 10.8-1.pgdg90+1)
-- Dumped by pg_dump version 10.7 (Ubuntu 10.7-0ubuntu0.18.04.1)

     ...

--
-- PostgreSQL database dump complete
--
```

#### Restore database dump
Build the image using `docker-compose` file. The containers for `postgres:10` and `pgadmin` should be built successfully while the container for `web` will fail because there is no data in the postgres database yet. 
Inspect the "Destination" key of the postgres 10 container
```bash
docker container ls #get name and ID of postgres 10 container
docker inspect -f '{{ json .Mounts }}' <container_id> | python -m json.tool #substitute corresponding container ID
```
Copy the dump to the "Destination" volume path of postgres 10 container
- SYNTAX:
```bash
docker cp <path-to-dump> <container-name>:<Destination-path>
```
- Example:
```bash
docker cp db_dump/datalog_db.dmp postgres-pgdmin_postgres_1:/var/lib/postgresql/data
```
Restore the database from the dump by going inside the container and run `psql` client command
```bash
docker exec -it postgres-pgdmin_postgres_1 bash   #go inside container
psql -U postgres -d datalog_db -f /var/lib/postgresql/data/datalog_db.dmp   #restore db dump
exit    #exit from docker exec
```
The 3 commands above can be combined into 1 command as:
```bash
docker exec postgres-pgdmin_postgres_1 psql -U postgres -d datalog_db -f /var/lib/postgresql/data/datalog_db.dmp 
```
### Export database script (.sql) using pgAdmin Backup tool
#### Backup database in pgAdmin
Right click on the server --> select **Backup Server**
![pgadmin_backup](../media/pgadmin_backup_server.PNG)
Check the pop-up to see where the database script is exported to. By default it is stored as `/var/lib/pgadmin/storage/***/dump.sql` where `***` is the pgadmin username.
To retrieve this script (which is currently inside the pgadmin container) and mount it to folder `db_dump`, use the `docker copy` command:
```bash
docker cp webapp_dbaccess_pgadmin_1:/var/lib/pgadmin/storage/***/dump.sql db_dump/
```
### Make new Dockerfile for postgres to copy the database script to the init folder of the postgres container
The official postgres docker image will run .sql scripts found in the `/docker-entrypoint-initdb.d/` folder. Create a new dockerfile for postgres `dockerfile-postgres` and add the following content:
```bash
FROM postgres:10
ADD dump.sql /docker-entrypoint-initdb.d/
```
In the `docker-compose` file under the configuration of postgres container, add the following lines:
```bash
...
  postgres:
    ...
    build:
      context: .
      dockerfile: dockerfile-postgres
    ...
```
~~Not yet working...~~ Finally worked!
The app is now running at [https://dash.datalog.live/dash/](https://dash.datalog.live/dash/)

