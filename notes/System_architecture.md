### System architecture
![project architecture](../media/project_structure.png)

![software architecture](../media/software_architecture.png)

### Database structure
![data model](../media/data_model.png)

#### *Unique identifier

### Structure of json dictionary sent by ESP32 to API Gateway 
![json struct](../media/json_data_structure.png)