# Installation

## Download MicroPython binaries

[All MicroPython binaries](https://micropython.org/download#esp32)

[esp32-20190529-v1.11.bin](https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin)

[Flash MicroPython](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html)


```bash
sudo adduser <USERNAME> dialout
sudo login -f <USERNAME>
pip install esptool
wget -O esp32-20190529-v1.11.bin https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190529-v1.11.bin
sudo screen /dev/ttyUSB0 115200

```

## Pin out

![ESP32 pin out](../media/esp32_pinout.jpg)



## IDE 
[Thonny IDE](https://randomnerdtutorials.com/getting-started-thonny-micropython-python-ide-esp32-esp8266/)

```bash
sudo apt install python3 python3-pip python3-tk
sudo pip3 install thonny
```