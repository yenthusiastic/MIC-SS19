from time import sleep_ms, sleep
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C

i2c = I2C(-1, Pin(4),Pin(5),freq=40000) # Bitbanged I2C bus
assert 60 in i2c.scan(), "No OLED display detected!"

screen = SSD1306_I2C(128, 64, i2c)
screen.invert(1) # White text on black background
#screen.contrast(50)


def set_contrast(c=255):
    screen.contrast(c)

def put_text(txt, x=0, y=0):
    #screen.fill(0)
    screen.text(str(txt), x, y)
    screen.show()
    #sleep_ms(40)

def blink_screen(delay=1, step=1):
    screen.invert(1)
    for i in range(0,255, +step):
        screen.contrast(i)
        sleep_ms(delay)
    for i in range(255,-1, -step):
        screen.contrast(i)
        sleep_ms(delay)
    screen.invert(0)

#set_contrast(255) # Maximum contrast
#put_text("datalog.live",18,30)
