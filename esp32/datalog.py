import esp
esp.osdebug(None)
import gc
gc.collect()
import sys
from machine import Pin
from time import sleep
import dht
import screen as oled
import utime as time
import adafruit_gps
from machine import UART
import credentials

try:
  import usocket as socket
except:
  import socket
try:
  import urequests as requests
except:
  import requests
import network

# =====CONFIG========
NODE_ID = 6
SENSOR_ID_GPS = 12
SENSOR_ID_DHT22 = 13
REQ_URL = "http://req.datalog.live:5560"

REQ_INTERVAL = 15000  # in ms
UPDATE_INTERVAL = 1000
# ===================


# GPS serial connection
uart = UART(1, 9600)                         # init with given baudrate
uart.init(9600, bits=8, parity=None, stop=1, tx=12, rx=13) # init with given parameters

wifi = None

# sensor
sensor = dht.DHT22(Pin(15))
led = Pin(2, Pin.OUT)
btn = Pin(0, Pin.IN, Pin.PULL_UP)


def connect_wifi(ssid, psk):
    import network
    global wifi
    wifi = network.WLAN(network.STA_IF)
    wifi.active(True)
    wifi.connect(ssid, psk)
    #while wifi.isconnected() == False:
    #    sleep(1)
    #    if btn.value() == 0:
    #        print("Not waiting fro Wifi...")
    #        break
    if wifi.isconnected():
        print("Connected: ",wifi.ifconfig()[0])


def http_get(url):
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    while True:
        data = s.recv(100)
        if data:
            print(str(data, 'utf8'), end='')
        else:
            break
    s.close()


def read_dht():
  global temp, hum
  temp = hum = 0
  try:
    sensor.measure()
    temp = sensor.temperature()
    hum = sensor.humidity()
    if (isinstance(temp, float) and isinstance(hum, float)) or (isinstance(temp, int) and isinstance(hum, int)):
      msg = (b'{0:3.1f},{1:3.1f}'.format(temp, hum))
      hum = round(hum, 2)
      return(msg).decode("utf-8").split(',')
    else:
      return None#('Invalid sensor readings.')
  except OSError as e:
    print('Failed to read sensor: ', e)
    return None


def read_sensor_values():
    val = read_dht()
    if val:
        val_str = "T:" + val[0] +"C H:" + val[1] + "%"
    else:
        val_str = "Sensor Error"
    
    if gps.latitude is None or not gps.has_fix:
        lat = "No"
        lon = "GPS"
    else:
        lat = gps.latitude
        lon = gps.longitude
    return (val, val_str, lat, lon)


def update_display(val, lat, lon, req_status):
    try:        
        status_line_str = "{:2.0f}".format( (REQ_INTERVAL - time.ticks_diff(cur_req, last_req))/1000 )
        if wifi.isconnected() == False:
            status_line_str="No WiFi Conn  " + status_line_str
        elif val is None:
            status_line_str="Sensor Error  " + status_line_str
        elif req_status is None:
            #req_status[0] == 1:
            status_line_str="Request Error " + status_line_str
        else:
            status_line_str="datalog.live  " + status_line_str
        
        oled.screen.fill(0)
        oled.screen.text(status_line_str,0,2)
        oled.screen.text("_________________", 0, 4)
        oled.screen.text("NodeID:{0}".format(NODE_ID),0,17)
        oled.screen.text("GPS:{0}, DHT:{1}".format(SENSOR_ID_GPS, SENSOR_ID_DHT22),0,27)
        oled.screen.text('{0} {1}'.format(lat, lon), 0, 43)
        oled.screen.text(val_str,0,55)
        oled.screen.show()
        
        print('=' * 40)  # separator line.
        print('{0} {1}'.format(lat, lon))
        print(val_str)
        print("{:2.0f}".format( (REQ_INTERVAL - time.ticks_diff(cur_req, last_req))/1000) )
    except Exception as e:
        print("Exception at update_display: ", e)


def send_request(dht, gps_lat, gps_lon):
    global wifi
    try:
        req_status = None
        print("sending request...")
        if wifi.isconnected() and dht and gps_lat is not None:
            json_data = [{"sensorID": SENSOR_ID_DHT22, "data": [dht[0], dht[1]]}, {"sensorID": SENSOR_ID_GPS, "data": [gps_lat, gps_lon]}]
            req = requests.post(REQ_URL, json=json_data)
            req_status = [req.status_code, req.reason]
        else:
            print("Sensor Data Error, not sensing request.") 
        if req_status is not None:
            if req_status[0] is not 1:
                oled.put_text(req_status[0],102,16)
                if req_status[0] == 200:
                    led.on()
                    sleep(0.1)
                    led.off()
                    return True
        else:
            return False
        print(req_status)
    except Exception as e:
        print("Exception at request: ", e)
        print(req_status)


def init():
    try:
        global oled
        #oled.set_contrast(255) # Maximum contrast
        oled.screen.fill(0)
        oled.put_text("datalog.live",2,2)
        oled.put_text("Hello",10,20)
        oled.put_text("Mr. Mirza",10,30)
        sleep(2)

        if btn.value() == 0:
            print("sys.exit")
            sys.exit()
        
        connect_wifi("OnePlus5", "andyandy")
        #connect_wifi(credentials.ssid, credentials.password)
        
        gps = adafruit_gps.GPS(uart)
        gps.send_command('PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')
        gps.send_command('PMTK220,1000')
        sleep(1)
        return gps
    except Exception as e:
        print("Exception at init: ", e)


# main
try:
    req_status = 1
    gps = init()
    last_print = time.ticks_ms()
    last_req = time.ticks_ms()
        
    while True:
        cur_print = time.ticks_ms()
        cur_req = time.ticks_ms()
        gps.update()
        
        if time.ticks_diff(cur_print, last_print) >= UPDATE_INTERVAL:
            last_print = cur_print
            val, val_str, lat, lon = read_sensor_values()
            print(val, val_str, lat, lon)
            update_display(val, lat, lon, req_status)
        
        if time.ticks_diff(cur_req, last_req) >= REQ_INTERVAL:
            last_req = cur_req
            req_status = send_request(val, lat, lon)
            
except Exception as e:
        print("Exception at main: ", e)



