## Build logs of Web application UI
Summary of front-end web app developments

Current web app is running at [dash.datalog.live/dash/](dash.datalog.live/dash/)

All image builds are pushed to this [Docker Hub repository](https://hub.docker.com/r/yenthusiastic/datalog-webgui).

Build log:

Date | Build tag | Description
---------|----------|---------
22.05.2019 | b220519 | Possibility to filter sensors by type (GPS, temperature) and sensorID (1,2,3,etc.). Show map for GPS sensors and line chart for temperature sensors 
31.05.2019 | b310519 | Display detailed sensor data in a table by clicking on marker. Create database dump and manage to fire postgres container which initializes data from the dump
08.06.2019 | b080619 | Possibility to select several markers at the same time to display data in table. Hovering on or selecting single node will also display temperature and humidity data in line charts. 
17.06.2019 | b170619 | Selecting a single node will show the complete past track of the node based on GPS data

### 22.05.19
![webapp_build220519_1.png](../media/webapp_build220519_1.png)
![webapp_build220519_2.png](../media/webapp_build220519_2.png)

### 31.05.19
![webapp_build250519.png](../media/webapp_build310519.png)

### 08.06.19
[webapp_build070619.pdf](../media/b080619.pdf)
<break>
![webapp_build080619_1](../media/webapp_build080619_1.PNG)

![webapp_build080619_2](../media/webapp_build080619_2.PNG)

### 17.06.19
![webapp_build170619](../media/webapp_build170619.PNG)