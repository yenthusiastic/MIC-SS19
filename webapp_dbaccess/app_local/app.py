import dbaccess as db
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output, State
import flask
from textwrap import dedent as d
import dash_table as dtable
import json

server = flask.Flask(__name__)

gps_df_header = ["Sensor ID", "Timestamp", "Latitude", "Longitude", "Node ID"]
mapbox_access_token = "pk.eyJ1IjoieWVudGh1bmd1eWVuIiwiYSI6ImNqdmZvNXQ5djBqYmc0ZnFwbGlicWplcHIifQ.CdvxuFW09O2tz0IE-urA4Q"

dbconn = db.connect()
all_sensors = db.get_sensor_types(dbconn)
gps_data = db.get_gps_data(dbconn)
gps_df = pd.DataFrame(gps_data, columns = gps_df_header)
print(gps_df)
last_nodeID = 7
last_node_pos = gps_df[gps_df["Node ID"] == last_nodeID]


@server.route('/')
def index():
    #return 'Hello Flask app'
    return '<a href="https://dev.datalog.live/dash/">Datalog.live App</a>'

external_stylesheets = [
    'https://codepen.io/chriddyp/pen/bWLwgP.css'
]

app = dash.Dash(__name__, 
                server=server, 
                routes_pathname_prefix='/dash/',
                external_stylesheets=external_stylesheets)

styles = {
    'map-box': {
        'border': 'thin lightgrey solid',
    }
}

def datalog_layout():
    return html.Div([
    html.Div([
        html.H2("Sensor Data Visualization")
    ]),
    html.Div([
        html.H6("Click on markers on the map to view detailed data of a node."),
        
        ]),
    html.Div([
        dcc.Graph(
            id='mapbox',
            figure={
                'data': [{
                    'lat': gps_df.Latitude, 
                    'lon': gps_df.Longitude, 
                    'type': 'scattermapbox', 
                    'mode': 'markers',
                    'marker': {'size': 20, 'color':'orange'},
                    'text': ["Node {}".format(nodeID for nodeID in gps_df["Node ID"])],
                    'hoverinfo': 'lat' + 'lon'
                }],
                'layout': {
                    'autosize': True,
                    'hovermode': 'closest',
                    'showlegend': False,
                    'mapbox': {
                        'accesstoken': mapbox_access_token,
                        'bearing': 0,
                        'center': {'lat': np.asscalar(last_node_pos["Latitude"].iloc[0]), 'lon': np.asscalar(last_node_pos["Longitude"].iloc[0])},
                        'pitch': 5, 'zoom': 10,
                    },
                    'margin': {
                        'l': 0, 'r': 0, 'b': 0, 't': 0
                    },
                    'clickmode': 'event+select',
                    'uirevision': 'static'
                }
            }
        )
    ], style =styles["map-box"]),
    html.Div(id='table_area', style = {"padding-left":"2%", "padding-right":"2%", 'padding-bottom':'20px', 'padding-top':'20px'}),
    html.Div(id='linechart_area', style={'padding':'10px'}),
    dcc.Interval(
            id='interval-component',
            interval=1*200, # in milliseconds
            n_intervals=0
    ),
    html.Div(id="hidden_div", style={"display":"none"})

],className="container", style={'color':'steelblue'})

app.layout = datalog_layout

#callback to fetch new data every 200ms
@app.callback(
    Output('hidden_div', 'children'),
    [Input('interval-component', 'n_intervals')])
def fetch_new_data(n):
    global gps_data, gps_df, node1_pos
    gps_data = db.get_gps_data(dbconn)
    gps_df = pd.DataFrame(gps_data, columns = gps_df_header)
    #print(gps_df)
    node1_pos = gps_df[gps_df["Node ID"] == 1]
    return None


#callback to display data of a selected marker in a table, including Node ID, sensor ID, timestamp, latitude, longitude
@app.callback(
    Output('table_area', 'children'),
    [Input('mapbox', 'selectedData'),
     Input('mapbox', 'clickData'),
    Input('mapbox', 'hoverData')])
def display_click_data(selectedData, clickData, hoverData):
    if (clickData is not None or hoverData is not None) and selectedData is None:
        # clickData["points"][0] is a dict: {'curveNumber': 0, 'pointNumber': 7, 'pointIndex': 7, 'lon': 5.6992, 'lat': 51.5157, 'text': 'Sensor 6'}   
        # print(clickData)
        global last_nodeID
        if clickData is not None:
            try:
                last_nodeID = int(clickData["points"][0]['text'].split(" ")[1])  # get the node ID from 'text' value of the clicked point
            except:
                print("Not applicable")
        elif hoverData is not None:
            try:
                last_nodeID = int(hoverData["points"][0]['text'].split(" ")[1]) 
            except:
                print("Not applicable")
        print("single marker {} selected".format(last_nodeID))
        # get the data row of the selected node ID from gps_df table and convert to pandas dataframe
        df = pd.DataFrame(gps_df[gps_df["Node ID"] == last_nodeID])   
        gps_sensor_id = np.asscalar(df['Sensor ID'].iloc[0])
        # format date time
        df['Timestamp'] = pd.to_datetime(df["Timestamp"].dt.strftime('%Y.%m.%d %H:%M:%S')) 
        datetime = df["Timestamp"].dt.strftime('%Y.%m.%d %H:%M:%S').iloc[0].split(" ")
        # get all temperature and humidity data belonging to selected node
        temp_data = db.get_node_details(dbconn, last_nodeID, df.iloc[0]["Timestamp"])
        temp_df = pd.DataFrame(temp_data, columns = ["Temperature sensor ID", "Temperature (°C)", "Humidity (%)"])
        if len(temp_df["Temperature sensor ID"]) > 1:
            temp_df["Latitude"] = len(temp_df["Temperature sensor ID"])*[df["Latitude"]]
            temp_df["Longitude"] = len(temp_df["Temperature sensor ID"])*[df["Longitude"]]
        else: 
            temp_df["Latitude"] = [df["Latitude"]]
            temp_df["Longitude"] = [df["Longitude"]]        
        return [html.H6('Most recent data of node {}, GPS sensor {}:'.format(last_nodeID, gps_sensor_id)),
                html.P('Recorded on {} at {}'.format(datetime[0], datetime[1])),
                html.Div(dtable.DataTable(
				id='dtable',
				columns=[{"name": i, "id": i} for i in list(temp_df.columns.values)],
                style_cell={'textAlign': 'left', 'width': '20%', 'font_size': '15px','padding-left':"5px"},
                style_cell_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(240,240,240)'
                }],
				data=temp_df.to_dict("rows"))),
               ]
    elif selectedData is not None:
        print("several markers selected")
        print(json.dumps(selectedData, indent=2))
        selectedIDs = []
        columns = ["Node ID", "Latitude", "Longitude", "Timestamp", "Temperature sensor ID", "Temperature (°C)", "Humidity (%)"]
        full_table = None
        for point in selectedData["points"]:
            try:
                #print(point['text']) 
                selectedIDs.append(int(point['text'].split(" ")[1])) # get the sensor ID from 'text' value of the selected points
            except:
                print("Ignore first point in selected points")
        print(selectedIDs)
        if len(selectedIDs) >= 1:
            for index, selectedID in enumerate(selectedIDs):
                    single_df = pd.DataFrame(gps_df[gps_df["Node ID"] == selectedID])   # get the data row of the selected node ID and convert to pandas dataframe
                    
                    single_df['Timestamp'] = pd.to_datetime(single_df["Timestamp"].dt.strftime('%Y.%m.%d %H:%M:%S')) 
                    print(single_df)
                    temp_data = db.get_node_details(dbconn, selectedID, single_df.iloc[0]["Timestamp"])
                    temp_df = pd.DataFrame(temp_data, columns = ["Temperature sensor ID", "Temperature (°C)", "Humidity (%)"])
                    
                    if len(temp_df["Temperature sensor ID"]) > 1:
                        temp_df["Node ID"] = len(temp_df["Temperature sensor ID"])*[single_df["Node ID"]]
                        temp_df["Latitude"] = len(temp_df["Temperature sensor ID"])*[single_df["Latitude"]]
                        temp_df["Longitude"] = len(temp_df["Temperature sensor ID"])*[single_df["Longitude"]]
                        temp_df["Timestamp"] = len(temp_df["Temperature sensor ID"])*[single_df['Timestamp']]
                    else: 
                        temp_df["Node ID"] = [single_df["Node ID"]]
                        temp_df["Latitude"] = [single_df["Latitude"]]
                        temp_df["Longitude"] = [single_df["Longitude"]]
                        temp_df["Timestamp"] = [single_df['Timestamp']]
                    # reorder the columns of the dataframe
                    temp_df = temp_df[columns]
                    if index == 0:
                        full_table = pd.DataFrame(temp_df, columns=columns)
                    else:
                        full_table = full_table.append(temp_df)
            print(full_table)
            return [
                html.H6('Most recent data of selected nodes:'),
                html.Div(dtable.DataTable(
                id='dtable',
                data=full_table.to_dict("rows"),
                columns=[{"name": i, "id": i} for i in list(full_table.columns.values)],
                style_cell={'textAlign': 'left', 'width': '15%', 'font_size': '15px'}, 
                style_cell_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(240,240,240)'
                },
                {
                    'if': {'column_id': "Temperature sensor ID"},
                    'textAlign': 'right'
                },
                {
                    'if': {'column_id':  "Temperature (°C)"},
                    'textAlign': 'right'
                },
                {
                    'if': {'column_id': "Humidity (%)"},
                    'textAlign': 'right'
                }],
            ))]
   
    else:
        return None

#callback to display humidity and temperature data of a selected marker as line charts
@app.callback(
    Output('linechart_area', 'children'),
    [Input('mapbox', 'clickData')])
def show_line_chart(clickData):
    if clickData is not None:
        temp_traces = []
        humid_traces = []
        global last_nodeID
        # clickData["points"][0] is a dict: {'curveNumber': 0, 'pointNumber': 7, 'pointIndex': 7, 'lon': 5.6992, 'lat': 51.5157, 'text': 'Sensor 6'}   
        try:
            last_nodeID = int(clickData["points"][0]['text'].split(" ")[1])  # get the node ID from 'text' value of the clicked point
        except:
            print("Not applicable")
        print("marker selected: {}".format(last_nodeID))
        node_temp_sensors = db.get_node_sensors(dbconn, last_nodeID, "temp")
        for sensorID in node_temp_sensors:
            df_header = ["Timestamp", "Temperature"]
            df = pd.DataFrame(db.get_temperature(dbconn, sensorID), columns=df_header)
            #df["Timestamp"] = pd.to_datetime(df["Timestamp"]).dt.tz_localize('UTC').dt.tz_convert("Europe/Berlin")
            temp_traces.append(go.Scatter(
                    x = df["Timestamp"],
                    y = df["Temperature"],
                    mode = 'lines+markers',
                    name = 'Temperature, sensorID: {}'.format(sensorID)
                ))
            df_header = ["Timestamp", "Humidity"]
            df = pd.DataFrame(db.get_humidity(dbconn, sensorID), columns=df_header)
            humid_traces.append(go.Scatter(
                    x = df["Timestamp"],
                    y = df["Humidity"],
                    mode = 'lines+markers',
                    name = 'Humidity, sensorID: {}'.format(sensorID),
                    yaxis='y2', 
                    xaxis='x'
                ))
        return [
            html.H6('Temperature (°C) graph of node {} '.format(last_nodeID)),
            dcc.Graph(
                figure={'data': temp_traces + humid_traces,
                'layout':{
                            'xaxis':{'title': 'Date time'},
                            'yaxis':{'title': 'Temperature (°C)'},
                            'yaxis2':{'title': 'Humidity (%)', 'overlaying':'y', 'side':'right'}
                        }
                }
            ),
           
           
        ]

    return None
    
    

#callback to move map's center point to selected marker and highlight the marker
@app.callback(
    Output("mapbox", "figure"),
    [Input('mapbox', 'clickData')]
)
def update_figure(clickData):
    global last_nodeID
    bounds = []
    if clickData is not None:
        try:
            last_nodeID = int(clickData["points"][0]['text'].split(" ")[1])  # get the node ID from 'text' value of the clicked point
        except:
            print("Not applicable")
        #print(last_nodeID)
    node_gps_sensor = db.get_node_sensors(dbconn, last_nodeID, "gps")
    #print("node gps sensor: {}".format(node_gps_sensor))
    gps_track_data = db.get_location_track(dbconn, node_gps_sensor)
    gps_track_df = pd.DataFrame(gps_track_data, columns = ["Latitude", "Longitude", "Timestamp"])
    gps_track_df['Timestamp'] = pd.to_datetime(gps_track_df["Timestamp"].dt.strftime('%Y.%m.%d %H:%M:%S')) 
    #print(gps_track_df)
    last_pos = [np.asscalar(gps_track_df["Latitude"].iloc[0]), np.asscalar(gps_track_df["Longitude"].iloc[0])]
    return {
        'data': [
            {
                    'lat': gps_df.Latitude, 
                    'lon': gps_df.Longitude, 
                    'type': 'scattermapbox', 
                    'mode': 'markers',
                    'marker': {'size': 20, 'color':'orange'},
                    'text': ["Node {}".format(nodeID) for nodeID in gps_df["Node ID"]],
                    'name': "Last location",
                    'hoverinfo': 'lat' + 'lon' 
            },
            {
                    'lat': gps_track_df.Latitude, 
                    'lon': gps_track_df.Longitude, 
                    'type': 'scattermapbox', 
                    'mode': 'lines+markers',
                    'marker': {'size': 12, 'color':'steelblue'},
                    'text': gps_track_df.Timestamp,
                    'name': "Node {} track".format(last_nodeID),
                    'hoverinfo': 'lat' + 'lon'
            },
            ],
        "layout": go.Layout(
                autosize=True,
                hovermode='closest',
                showlegend=False,
                mapbox={'accesstoken': mapbox_access_token,
                        'bearing': 0,
                        'center': {'lat': last_pos[0], 'lon': last_pos[1]},
                        'pitch': 5, 'zoom': 10,
                        },
            
                uirevision="static",
                margin={
                        'l': 0, 'r': 0, 'b': 0, 't': 0
                        },
        )

    }
    

if __name__ == '__main__':
    app.run_server(debug=True,host='0.0.0.0')

