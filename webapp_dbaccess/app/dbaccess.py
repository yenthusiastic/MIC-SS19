import psycopg2
from configparser import ConfigParser
import sys
import numpy as np
import datetime

db_host='postgres'
port=5432
database='datalog_db'
user='postgres'
password='postgres'

conn = None

def connect():
    """ Connect to the PostgreSQL database server """
    global conn
    try:
        # read configs from config file
        # db_params = read_config(config_file, section_name)
        
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host=db_host, port=port, user=user, password=password, database=database)
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        print("Successfully connected to DB")
        return conn

def get_sensor_types(dbconn):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT DISTINCT "sensorType" FROM public."SENSORS"' 
        cur.execute(query)

        # display response from DB
        res = [r[0] for r in cur.fetchall()]
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res

def get_sensor_IDs(dbconn, sensorType):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'select "sensorID" from public."SENSORS" where "sensorType" = \'{}\''.format(sensorType)
        cur.execute(query)

        # display response from DB
        res = [r[0] for r in cur.fetchall()]
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res

def get_temperature(dbconn, sensorID):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT "timestamp", "temperature" from public."TEMPC_HUMID_DATA" WHERE "sensorID" = {} ORDER BY "timestamp" DESC'.format(sensorID)
        cur.execute(query)

        # display response from DB
        res = cur.fetchall()
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res
    
def get_humidity(dbconn, sensorID):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT "timestamp", "humidity" from public."TEMPC_HUMID_DATA" WHERE "sensorID" = {} ORDER BY "timestamp" DESC'.format(sensorID)
        cur.execute(query)

        # display response from DB
        res = cur.fetchall()
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res
    

def get_gps_data(dbconn):
    res = None
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT * FROM public."GPS_last_location"'
        cur.execute(query)

        # display response from DB
        res = cur.fetchall()
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res

def get_node_IDs(dbconn, sensorIDs):
    all_sensors = []
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        for sensorID in sensorIDs:
            query = 'SELECT "nodeID" FROM public."NODES" WHERE \'{}\'=ANY("sensorID")'.format(sensorID)
            cur.execute(query)

            # display response from DB
            all_sensors.append(cur.fetchone())
            print(all_sensors)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return all_sensors

    
# get all temperature/ humidity sensors within a given node ID    
def get_node_sensors(dbconn, nodeID, sensorType):
    sensors = []
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT "sensorID" FROM public."NODES" where "nodeID" = {}'.format(nodeID)
        cur.execute(query)

        # display response from DB
        res = cur.fetchone()
        print(res)
        
        # close the communication with the DB
        cur.close()
        sensors = res[0]
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        if sensorType == "temp":
            return is_temp_sensor(dbconn, sensors)
        elif sensorType == "gps":
            return is_gps_sensor(dbconn, sensors)
        else:
            return None

# get all past and current locations of a node given the GPS sensor ID of the node        
def get_location_track(dbconn, sensorID):
    all_locations = []
    try:
        # create a cursor
        cur = dbconn.cursor()
        
        # execute a statement
        query = 'SELECT "latitude", "longitude", "timestamp" from public."GPS_DATA" where "sensorID" = {} ORDER BY "timestamp" DESC'.format(sensorID)
        cur.execute(query)

        # display response from DB
        all_locations = cur.fetchall()
        #print(all_locations)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return all_locations

def is_gps_sensor(dbconn, sensorIDs):
    gps_sensor = 0
    try:
        # create a cursor
        cur = dbconn.cursor()
        for sensorID in sensorIDs:
            query = 'SELECT exists (SELECT 1 FROM "GPS_DATA" WHERE "sensorID" = {} LIMIT 1)'.format(sensorID)
            cur.execute(query)
            res = cur.fetchone()
            if res[0] == True:
                gps_sensor = sensorID
            # close the communication with the DB
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return gps_sensor
    
    
def is_temp_sensor(dbconn, sensorIDs):
    temp_sensors = []
    try:
        # create a cursor
        cur = dbconn.cursor()
        for sensorID in sensorIDs:
            query = 'SELECT exists (SELECT 1 FROM "TEMPC_HUMID_DATA" WHERE "sensorID" = {} LIMIT 1)'.format(sensorID)
            cur.execute(query)
            res = cur.fetchone()
            if res[0] == True:
                temp_sensors.append(sensorID)
            # close the communication with the DB
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return temp_sensors
        
    
def get_node_details(dbconn, nodeID, timestamp):
    res = None
    query = ""
    node_temp_sensors = get_node_sensors(dbconn, nodeID, "temp")
    if len(node_temp_sensors) > 1: 
        query = 'SELECT DISTINCT ON ("TEMP_TABLE"."sensorID") "TEMP_TABLE"."sensorID", "TEMP_TABLE"."temperature", "TEMP_TABLE"."humidity" FROM (SELECT * from public."TEMPC_HUMID_DATA" where "sensorID" in {} AND "timestamp" >= \'{}\' ORDER BY "timestamp" DESC) AS "TEMP_TABLE" '.format(tuple(node_temp_sensors), timestamp)
    else:
        query = 'SELECT DISTINCT ON ("TEMP_TABLE"."sensorID") "TEMP_TABLE"."sensorID", "TEMP_TABLE"."temperature", "TEMP_TABLE"."humidity" FROM (SELECT * from public."TEMPC_HUMID_DATA" where "sensorID" = {} AND "timestamp" >= \'{}\' ORDER BY "timestamp" DESC) AS "TEMP_TABLE"'.format(node_temp_sensors[0], timestamp)
    try:
        # create a cursor
        cur = dbconn.cursor()
        # execute query statement
        cur.execute(query)
        #print(query)
        
        # fetch response from DB
        res = cur.fetchall()
        #print(res)
       
        # close the communication with the DB
        cur.close()
    
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit()
    finally:
        return res
    
def read_config(filename, section):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        db_params = parser.items(section)
        for param in db_params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db
 

