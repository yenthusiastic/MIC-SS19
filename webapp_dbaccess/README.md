### Building and running web application and postgres containers
- In current `webapp_dbaccess` folder, build and run containers using `docker-compose` with detached mode: 
```bash
docker-compose up --build -d
```
App will run on `localhost:5000/dash`
- Check that 3 containers should be built using the command `docker ps`:
```
CONTAINER ID        IMAGE                 COMMAND                  CREATED              STATUS                        PORTS                           NAMES
9efb52c5dddc        dpage/pgadmin4        "/entrypoint.sh"         5 minutes ago        Up 5 minutes         443/tcp, 0.0.0.0:5555->80/tcp   webapp_dbaccess_pgadmin_1
94f758e9fd27        webapp_dbaccess_web   "python3 -u app/app.…"   3 minutes ago        Up 3 minutes         0.0.0.0:5000->8050/tcp          webapp_dbaccess_web_1
94a2d17a3202        postgres:10           "docker-entrypoint.s…"   5 minutes ago        Up 5 minutes         0.0.0.0:6543->5432/tcp          webapp_dbaccess_postgres_1
```
### Building images of web application and postgres database for remote pulling
- Build the images
```bash
docker build -f Dockerfile -t datalog_webgui .              # build web app image
docker build -f dockerfile-postgres -t datalog_db .         # build postgres image with database
```
- Tag and push the images
```bash
docker login                                                                    # login to Docker hub account
docker tag datalog_db:latest yenthusiastic/datalog:db_v0.1                      # tag the postgres image
docker tag datalog_api_gateway:latest yenthusiastic/datalog:api_gateway_v0.1    # tag the web app image
docker push yenthusiastic/datalog:db_v0.1                                       # push the postgres image
docker push yenthusiastic/datalog:api_gateway_v0.1                              # push the web app image
```
### Pulling and running the images on new machines
```bash
docker run -d -p 5432:5432 --name datalog_db datalog_db     # run image of postgres with current database
docker run -d -p 5000:8050 --link datalog_db:postgres datalog_webgui   # run the image of the web app and links it to the postgres container
```
