import requests as req
import urllib3
import json

api_token = "SECRET_TOKEN"
jdata = json.dumps({"msg": [ {"sensorID": '1', "data": '52.5157, 5.8992'},{"sensorID": '4', "data": '22.0, 50.0'}]})
headers = {'Content-Type': 'application/json',
           'Authorization': api_token}
r = req.post('http://req.datalog.live:5560', headers=headers, json=jdata)
print(r.status_code)
