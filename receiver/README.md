### Instructions for building request handler (API Gateway) image and running container
```bash
sudo docker build --tag=api_gateway .
sudo docker run -d -p 5560:5560 api_gateway
```
The app will serve on port 5560

### Instructions for sending dummy data to DB using the API Gateway (when real sensors are not available)
```bash
python request_client.py
```
This should return the HTTP OK <200> Response code.

Additionaly the files *req_handler.ipynb* and *request_client.ipynb* are provided for a convienend way of prototyping and development. 
They can be executed localy on a [JuypterLab](https://jupyterlab.readthedocs.io/en/stable/) local server. 
JupyterLab (JupyterNotebook) is a Interactive Python (IPython) based interactive programming framework.

### Instructions for tagging and publishing the image to Docker Hub
```bash
sudo docker image tag api_gateway yenthusiastic/datalog:api_gateway_v0.1
sudo docker image push yenthusiastic/datalog:api_gateway_v0.1
```