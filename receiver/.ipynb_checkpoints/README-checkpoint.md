### Instructions for building request handler container
```bash
sudo docker build --tag=req_handler .
sudo docker run -d -p 5560:5560 req_handler
```
The app will serve on port 5560