from http.server import BaseHTTPRequestHandler, HTTPServer 
import json
import requests as req
import urllib3
import psycopg2

ADDR = "0.0.0.0" #0.0.0.0 for public
PORT = 5560

conn = psycopg2.connect(host="db.iota.pw", port=6543, database="datalog_db", user="postgres", password="postgres")

DEBUG = True

class ReqHandler(BaseHTTPRequestHandler):
    def _set_headers(self, status_code):
        self.send_response(status_code)
        #self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write(b'<html><body><h1>hi!</h1></body></html>')
        print(">>>Request: \n{0}".format(self.requestline))
        print(">>>Headers: \n{0}".format(self.headers))

    def do_HEAD(self):
        self._set_headers(200)
        
    def do_POST(self):
        datalen = int(self.headers['Content-Length'])
        data = self.rfile.read(datalen)
        req_json = json.loads(data)
        if DEBUG:
            print(">>>Request: \n{0}".format(self.requestline))
            print(">>>Headers: \n{0}".format(self.headers))
            print(">>>JSON: \n{0}\n".format(req_json))        
        self._set_headers(200)
        # req_json
        self.data_handler(req_json)

    def connect(self, msg):
        global conn
        rtn = ""
        """ Connect to the PostgreSQL database server """
        try:
            cur = conn.cursor()
            cur.execute(msg)
            conn.commit()
            for x in cur.fetchall():
                print(x)
                rtn = x
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
                
        print('Database connection closed.')
        return rtn
            
    def data_handler(self, js):
        print("data_handler: \nJSON: {0}".format(js))
        for sen in js:
            s_id = sen["sensorID"]
            s_data = sen["data"]
            data_type = self.connect("""SELECT "dataType" FROM public."SENSORS" WHERE "sensorID" = '{0}' """.format(s_id))[0]
            if DEBUG:
                print("sensorID: {0}\ndataType: {1}\ndata:{2}".format(s_id, data_type, s_data))
            data_str = ""
            for x in s_data:
                data_str+= x + ","
            data_str = data_str[:-1]
            #db_ins = self.connect(""" INSERT INTO public."{0}" values (DEFAULT, {1}, current_timestamp, {2},  {3}) """.format(data_type, s_id, s_data[0], s_data[1]))
            db_ins = self.connect(""" INSERT INTO public."{0}" values (DEFAULT, {1}, current_timestamp, {2}) """.format(data_type, s_id, data_str))
        
        
try:
    server = HTTPServer((ADDR, PORT), ReqHandler)
    print("serving at port", PORT)
    server.serve_forever()
    #server.handle_request()
    server.socket.close()
except KeyboardInterrupt:
    print('Server shutdown...')
    server.shutdown()
finally:
    if conn is not None:
        conn.close()