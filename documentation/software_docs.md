## Documentation of software development
### Table of contents
1. [Project management](#section1)
2. [Software architecture](#section2)

   2.1. [Development tools](#section2.1)

   2.2. [ Software requirements](#section2.2)
3. [Relational database](#section3)

   3.1. [Database structure](#section3.1)

   3.2. [Database administration](#section3.2)

   3.3. [Get database dump of containerized Postgres database server](#section3.3)
4. [Software development and deployment](#section4)

   4.1. [Install Docker](#section4.1)

   4.2. [Copy the database dump to Postgres container](#section4.2)

   4.3. [Build web app and postgres database from Docker Compose file](#section4.3)

   4.4. [Create docker images for running on different hosts](#section4.4)
5. [Software development on sensor node](#section5)
6. [Remote server configuration](#section6)


### <a name="section1"></a> 1. Project management
The project uses Git as version control tool for all codes and documentation, and Docker Hub as the repository for all successfully built images.
 - <a href="https://gitlab.com/yenthusiastic/MIC-SS19" target="_blank">GitLab repo</a>
 - <a href="https://hub.docker.com/r/yenthusiastic/datalog" target="_blank">Docker Hub repo</a>.

### <a name="section2"></a> 2. Software architecture
#### <a name="section2.1"></a> 2.1. Development tools
The software uses the following frameworks and technology:
- **Docker** for building and deploying containerized apps
- **Dash** (Python) for building web application
- **Postgres & PgAdmin** for managing relational database schemes
- **Micropython** for embedded programming on sensor node

The following figure illustrates the software architecture of the project:

![soft_arch](../media/software_architecture.png)

#### <a name="section2.2"></a> 2.2. Software requirements
##### 2.2.1. Functional requirements

*Sensor node firmware*
- The firmware shall be able to retrieve sensor values from the temperature/humidity sensor(s) connected to the ESP32.
- The firmware shall be able to retrieve GPS location from the GPS module connected to the ESP32.
- The firmware shall be able to indicate/ visualize its current operational status.
- The firmware shall be able to send HTTPS requests containing sensor data to the API gateway.

*API Gateway*
- The API gateway shall be able to parse HTTPS requests sent by the ESP32 to obtain valid sensor data.
- The API gateway shall be able to connect to the database server.
- The API gateway shall be able to send valid sensor data to the corresponding data table in the database.

*Web application and database*
- The web application shall be able to connect to the database server. 
- The web application shall be able to retrieve relevant data from the database.
- The web application shall be able to display the most current status of all sensor nodes.
- User shall be able to interact with the web application.
- User shall be able to select and view detailed information of any sensor node. 
- The database shall be accessible through a user interface.

##### 2.2.2. Non-functional requirements
- The sensor node firmware shall be written in micropython
- The sensor node firmware shall be able to package all sensor data into a JSON object.
- The sensor node firmware shall send data to the API gateway at a regular interval of 15 seconds.
- The API gateway shall be written in Python3
- The API gateway shall be able to parse JSON object to obtain valid sensor data.
- The web application shall display the most current position of all sensor nodes as markers on a map.
- The web application shall be written in Python3 using Dash framework 
- The web application shall be able to provide following detailed information of a user-selected node:
   -  Node ID, ID of GPS module and ID of temperature/humidity sensor(s) on that node
   -  A table containing the last location coordinates and sensor values collected by the node with corresponding timestamp
   - A complete location track of the node on the map with exact latitude and longitude coordinates at each timestamp
   - A graph showing variation of temperature (°C) and humidity (%) values over time with 2 separate vertical axes.
- The web application shall refresh itself at a regular interval of 1 second.
- The database shall be hosted as a Postgres server inside a Docker container.
- The database shall be managed using PgAdmin's web interface.
- The API gateway and the web application shall run as microservices inside Docker containers.


### <a name="section3"></a> 3. Relational database

#### <a name="section3.1"></a> 3.1. Database structure
The database contains a single `SCHEMA` with following `TABLES`:
- `NODES`: this table contains the relevant information on all sensor nodes in the system's IoT network. New nodes have to be added to this table manually. The different methods for entering data into the tables are explained in the next sub-section.
- `SENSORS`: this table contains the relevant information on all sensors within all nodes in the system's IoT network. Like new nodes, new sensors also have to be added to this table manually. 
- `GPS_DATA`: this table contains all GPS locations with corresponding timestamp collected by all GPS sensors in the system's IoT network. 
- `TEMPC_HUMID_DATA`: this table contains all temperature (in °C) and humidity (in %) values  with corresponding timestamp collected by all environmental sensors in the system's IoT network.
- `TEMPF_HUMID_DATA`: this table contains all temperature (in °F) and humidity (in %) values  with corresponding timestamp collected by all environmental sensors in the system's IoT network

For the last 3 tables, new data is added into the tables automatically through the sensor node's firmware and the API Gateway.

The following figure illustrates the relational database model of the project:

![data_model](../media/data_model.png)

The tables are created using the following commands:
```sql
CREATE TABLE public."NODES" (
    "nodeID" integer NOT NULL,
    "nodeDescription" text NOT NULL,
    "sensorID" integer[]
);
CREATE TABLE public."SENSORS" (
    "sensorID" integer NOT NULL,
    "sensorType" text NOT NULL,
    "dataType" text NOT NULL,
    "sensorDescription" text NOT NULL
);
CREATE TABLE public."GPS_DATA" (
    index bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    latitude real NOT NULL,
    longitude real NOT NULL
);
CREATE TABLE public."TEMPC_HUMID_DATA" (
    index bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    temperature real NOT NULL,
    humidity real NOT NULL
);
CREATE TABLE public."TEMPF_HUMID_DATA" (
    index bigserial NOT NULL,
    "sensorID" integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    temperature real NOT NULL,
    humidity real NOT NULL
);
```

Addtionally, the database also contains a `VIEW` called `GPS_last_location` which contains only the last location with corresponding timestamp of every GPS sensor in the system's IoT network. This view is created using the following command:
```sql
 CREATE VIEW public."GPS_last_location" WITH (security_barrier='false') AS
 SELECT DISTINCT ON ("NODES"."nodeID") "GPS_DATA_1"."sensorID",
    "GPS_DATA_1"."timestamp",
    "GPS_DATA_1".latitude,
    "GPS_DATA_1".longitude,
    "NODES"."nodeID"
   FROM (( SELECT "GPS_DATA".index,
            "GPS_DATA"."sensorID",
            "GPS_DATA"."timestamp",
            "GPS_DATA".latitude,
            "GPS_DATA".longitude
           FROM public."GPS_DATA"
          ORDER BY "GPS_DATA"."timestamp" DESC) "GPS_DATA_1"
     JOIN public."NODES" ON (("GPS_DATA_1"."sensorID" = ANY ("NODES"."sensorID"))));
```

#### <a name="section3.2"></a> 3.2. Database administration
This section and the following section assumes that the Postgres database as well as PgAdmin are running inside Docker containers (see [Section 4](#section4) for installation instructions). In order to enter data into postgres database, there are 2 options:
##### Option 1: Enter data using PgAdmin web interface
Access the PgAdmin interface at [http://0.0.0.0:5555](http://0.0.0.0:5555) with following login credentials:

**user:** admin@thunguyen.cc
**pwd:** admin

Connect to Postgres server by clicking on *Add New Server*:

<img src = "../media/pgadmin_make_connection1.png" width=90%>

Under *Connection* tab, enter the following credentials: 

**user:** postgres 
**pwd:** postgres

<img src = "../media/pgadmin_make_connection2.png" width=30%>

Right click on the table that needs insertion of new data and choose **Query Tool**

<img src = "../media/pgadmin_3.png" width=40%>

On the panel on the right, type in the SQL query, for example:
```sql
INSERT INTO public."NODES" VALUES (1, 'ESP32 using WiFi. Sensors: GPS, DHT22.',	'{1,4}');
```
Finally, press F5 to execute the query.
##### Option 2: Enter data using psql CLI tool
To interact with the containerized postgres database through the postgres builtin CLI called `psql`, we first have to go inside the container using the following command:
```bash
docker exec -it webapp_dbaccess_postgres_1 bash
```
where `webapp_dbaccess_postgres_1` is the default name of the postgres container if built using instructions in **Section 4** (otherwise, use command `docker ps` to list all running containers and find out corresponding names).
Next, we connect to the `datalog_db` database on host `postgres` using the command:
```
root@postgres:/# psql -p 5432 -h postgres -U postgres -d datalog_db
```
Enter `postgres` when prompted for password to connect to the database. 

Upon successful connection, we can insert data using SQL query, for example:
```sql
INSERT INTO public."NODES" VALUES (1, 'ESP32 using WiFi. Sensors: GPS, DHT11.',	'{1,4}');
```

#### <a name="section3.3"></a> 3.3. Get database dump of containerized Postgres database server
When the database server is running inside a container, in order to get all current data stored inside the database and export as a dump (.sql) file, there are 2 options.
##### Option 1: Using pgadmin backup tool
Right click on the `postgres` server, then select **Backup Server...** and set a name for the file:

<img src = "../media/pgadmin_backup_server.PNG" width=80%>

Check the pop-up to see where the database script is exported to. By default it is stored as `/var/lib/pgadmin/storage/***/dump.sql` where `***` is the pgadmin username.
To retrieve this script (which is currently inside the pgadmin container) and mount it to folder `db_dump`, use the `docker copy` command:
```bash
docker cp webapp_dbaccess_pgadmin_1:/var/lib/pgadmin/storage/***/dump.sql db_dump/
```
##### Option 2: Using pgdump
###### Install postgres (corresponding version)
```bash
sudo apt-get install postgres:10
```
NOTE: specific postgres version has to be provided in above command if the postgres server running inside the Docker container has a version other than the latest version, so that the following commands could work
###### Dump postgres data from container to file:
  -  SYNTAX:
```bash
pg_dump -d <databasename> -h <hostname> -p <port> -U <username> -n <schemaname> -f <location of the dump file>
```
 - Example:
```bash
pg_dump -d datalog_db -h localhost -p 6543 -U postgres -n public -f datalog_db.dmp
```

### <a name="section4"></a> 4. Software development and deployment
The project is originally developed on a remote Virtual Private Server (VPS) running Ubuntu 18.04 LTS. Thus, the following instructions have only been tested on Linux platforms. However, except for the installtion of Docker & Docker Compose, all docker commands should work the same on a Windows machine with Docker for Windows installed. To install Docker for Windows (comes with Docker Compose), check the [official guide](https://docs.docker.com/docker-for-windows/install/).

The setup of the VPS is described in [Section 6](#section6).
#### <a name="section4.1"></a> 4.1. Install Docker
- install required dependencies and verify GPG key of Docker
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```

- install Docker Community Edition (CE)
```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce
```
- run the Hello World example with command `docker run hello-world` to verify installation:
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: ****
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
...
```
Use the `docker contaienr ls` or `docker ps` command to list all running containers. Then use the stop command to stop the container using its ID from above output
```bash
docker container stop <container-id>
```
Other useful docker commands:

```bash
docker container rm <container-id>  #remove a stopped container by its ID
docker image ls   #list all built or pulled images
docker image rm <image-id>    #remove a built or pulled image by its ID
docker volume ls  #list all local docker volumes
docker volume prune   #remove all local volumes not used by at least 1 container
docker system prune    #remove all stopped containers, dangling images, dangling networks
```

#### <a name="section4.2"></a> 4.2. Copy the database dump to Postgres container
The official postgres docker image will run `.sql` scripts found in the `/docker-entrypoint-initdb.d/` folder. In order to mount the exported database dump from [Section 3.3](#section3.3) onto the data folder of the container while building, a saparate Dockerfile for postgres has to be used. Here it is called `dockerfile-postgres` and has the following content:
```bash
FROM postgres:10
ADD dump.sql /docker-entrypoint-initdb.d/
ENV PGDATA=/data
```
In the main project's `docker-compose` file, under the configuration of postgres container, the following lines tell Docker Compose to use this specific Dockerfile while building postgres:
```bash
...
  postgres:
    ...
    build:
      context: .
      dockerfile: dockerfile-postgres
    ...
```

#### <a name="section4.3"></a> 4.3. Build web app and postgres database from Docker Compose file
The web application uses the Python script [app.py](https://gitlab.com/yenthusiastic/MIC-SS19/blob/master/webapp_dbaccess/app/app.py) to render the web interface and the [dbaccess.py](https://gitlab.com/yenthusiastic/MIC-SS19/blob/master/webapp_dbaccess/app/app.py) script under `app/` folder to communicate with the postgres database.

The additional files required for building the web app and postgres database with Docker Compose are:

|File                   |Function   |
|:--                    |:--        |
|Dockerfile             |instructions for building the web app image|
|requirements.txt       |list of all required libraries and dependencies, used by Dockerfile while building the web app image|
|dockerfile-postgres    |instructions for building postgres image mounted with all current data in the database|
|dump.sql               |a database dump of all current data in the database|
|docker-compose.yml     |define how the 3 containers (web app, postgres, pgadmin) should communicate with each other|

To build this multi-container application:

```bash
cd webapp_dbaccess
docker-compose up --build
```
The following error might appear:
> raise CSRFError(reason)
> pgadmin_1   | flask_wtf.csrf.CSRFError: 400 Bad Request: The CSRF session token is missing.

This is caused by a known issue in Flask that is not yet resolved. To avoid this, press `Ctrl+C` to stop docker-compose and rerun it in detached mode:
```bash
docker-compose up -d
```
Check that 3 containers should be built using the command `docker ps`:
```
CONTAINER ID        IMAGE                 COMMAND                  CREATED              STATUS                        PORTS                           NAMES
9efb52c5dddc        dpage/pgadmin4        "/entrypoint.sh"         5 minutes ago        Up 5 minutes         443/tcp, 0.0.0.0:5555->80/tcp   webapp_dbaccess_pgadmin_1
94f758e9fd27        webapp_dbaccess_web   "python3 -u app/app.…"   3 minutes ago        Up 3 minutes         0.0.0.0:5000->8050/tcp          webapp_dbaccess_web_1
94a2d17a3202        postgres:10           "docker-entrypoint.s…"   5 minutes ago        Up 5 minutes         0.0.0.0:6543->5432/tcp          webapp_dbaccess_postgres_1
```
The web app will be available at http://0.0.0.0:5000/dash/

#### <a name="section4.4"></a> 4.4. Create docker images for running on different hosts
##### Image of postgres database with all current sensor data
- Modify the `db_dump/dump.sql` file by removing the comment symbol `'--'` before the line `CREATE DATABASE datalog_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLA$`
- Build the image with a tag name
```bash
cd webapp_dbaccess
docker build -f dockerfile-postgres -t datalog_db .           # build the postgres image with all current data in database
```
- To run the image in detached mode and expose internal port 5432 for external communication:
```bash
docker run -d -p 5432:5432 --name datalog_db datalog_db       
```
- Tag and push the image to Docker hub:
```bash
docker tag datalog_db:latest yenthusiastic/datalog:db_v0.1    
docker push yenthusiastic/datalog:db_v0.1                   
```
- To check that there is data in the database, we can go inside the postgres database using command:
```bash
docker exec -it datalog_db bash #go inside the container
```
where `datalog_db` is the name of the container as specified by the `--name` flag in the above `docker run` command, and `bash` will allow us to execute system commands inside this container. To connect to the `datalog_db` database inside this container, we use the postgres built-in CLI called `psql`:
```
root@<container-id>:/# psql -p 5432 -U postgres -h 0.0.0.0 -d datalog_db
```
If the database is connected successfully, we will see `datalog_db=#` at the beginning of the command line
To check for data, execute a SQL command, for example:
```
datalog_db=# select * from public."SENSORS";
```
This should show all current sensors in the database.
To quit from `psql`, type `\q`. To get out of the container, type:
```bash
exit #exit from docker exec
```
##### Image of web application
- Build the image with a tag name
```bash
docker build -f Dockerfile -t datalog_webgui .
```
- To run the image and tell it to connect to the postgres database from above:
```bash
docker run -d -p 5000:8050 --link datalog_db:postgres datalog_webgui
```
The `datalog_db:postgres` mapping at the `--link` flag will tell the web app container to see the external `datalog_db` container as `postgres` inside the web app container. The web app will be available at http://0.0.0.0:5000/dash/.
- Tag and push the image to Docker hub:
```bash
docker tag datalog_webgui:latest yenthusiastic/datalog:webgui_v0.1
docker push yenthusiastic/datalog:webgui_v0.1
```
##### Image of API gateway
- Go to the receiver folder containing source code for API gateway
```bash
cd receiver/
```
- Build the image for the API Gateway using the Dockerfile in this folder
```bash
docker build -t datalog_api_gateway .
```
 Tag and push the image to Docker hub:
```bash
docker tag datalog_api_gateway:latest yenthusiastic/datalog:api_gateway_v0.1
docker push yenthusiastic/datalog:api_gateway_v0.1
```

### <a name="section5"></a> 5. Software development on sensor node
The firmare developed for the ESP32 is written in MicroPython, a Python implementation for microcontroller.
To enable execution of Python code on the ESP32, it has to be flashed with the MicroPython binaries.

The following code shows all commands necessary to flash a ESP32 on an Ubuntu Linux system. 
```bash
sudo adduser <USERNAME> dialout
sudo login -f <USERNAME>
pip install esptool
wget -O esp32-20190529-v1.11.bin https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190529-v1.11.bin
sudo screen /dev/ttyUSB0 115200
```
[Source: micropython.org](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html)



The firmare developed for the ESP32 microcontroller consists of multiple files, listed here:

|File           |Function   |
|:--            |:--        |
|main.py        |executed at start-up off the ESP32, imports datalog.py|
|datalog.py     |main programm handling all networking, requests and sensor IO
|credentials.py |contains WiFi credentials, ssid and psk|
|screen.py      |helper script for easy OLED display interaction, using ssd1306.py library|
|ssd1306.py     |library for OLED display|
|urequests.py   |requests library implementation for ESP32|
|adafruit_gps.py|library for GPS module|



The sensor data is packed into a JSON data structure of following format:

<img src = "../media/json_data_structure.png" width=60%>



### <a name="section6"></a> 6. Remote server configuration
The remote server used for development runs Ubuntu 18.04 LTS and has a domain name of **https://datalog.live/**. Here, different services can be accessed:
- [Jupyter Lab](https://lab.datalog.live/) where the Python scripts and Jupyter notebooks can be modified directly on the server through an interactive Python IDLE (Integrated development environment). A password is required to use this service.
- [pgAdmin](https://pg.datalog.live/) which provides a web interface for interacting with the postgres database as described in [Section 3.2](#section3.2). Login credentials are required to use this service.
- [Deployed Dash App](https://dash.datalog.live/dash): the web app deployed as Docker container
- [Development Dash App](https://dev.datalog.live/dash): the web app running locally on the remote host for development purposes.

Additionally, the server is also accessible over `SSH` on any local machine through SSH key pair authentication. All services are encrypted using `Let's Encrypt` certificates. 