## Documentation of hardware development 
## Node requirements
The minimal requirements for building a node are: 
 - nodeID: every node needs to be identifiable by a unique ID
 - sensorID: every sensor needs to be identifiable by a unique ID, only sensors can create data to be saved in the database
 - request: a node needs to be able to send a HTTP request to the API gateaway

## Prototype node implementation
This section gives instructions on how to build a prototype of the IoT nodes that can be used for this project.
### Components
 - Microcontroller: ESP32 with OLED display ([Wemos Lolin ESP32 OLED](https://www.banggood.com/LILYGO-ESP32-OLED-Module-For-Arduino-ESP32-OLED-WiFi-bluetooth-Dual-ESP-32-ESP-32S-ESP8266-p-1148119.html?cur_warehouse=UK))
 - Temperature/Humidity Sensor: [DHT22](https://www.banggood.com/AM2302-DHT22-Temperature-And-Humidity-Sensor-Module-For-Arduino-SCM-p-937403.html)
 - GPS Module: V.KEL TTL ([VK2828U7G5LF](https://www.banggood.com/1-5Hz-VK2828U7G5LF-TTL-GPS-Module-With-Antenna-p-965540.html))
 - Power supply: any 5V powerbank with microUSB port


The following figures show the current prototype in operation:

<img src="../media/node_hardware_full2.jpg"  height="400">

*Figure 1: Complete prototype node with all hardware components wired and working*

<img src="../media/node_hardware_screen2.jpg"  height="400">

*Figure 2: Close view of the OLED display showing information of the node while in operation*

### Explanation of OLED display

The top section of the OLED display is used as a status indicator bar.
In the top right corner, a timer shows the remaining time to the next request.
The indicator bar shows **datalog.live** in normal operation.
If an error state is detected, one of the following messages will be displayed:
 - **No WiFi Conn**:  No connection to WiFi network
 - **Sensor Error**:  A sensor returned no or invalid data
 - **Request Error**: A request timed out or returned a http code other than 200 (OK)

The lower part of the display shows the following information:
 - **NodeID**:        ID of the node
 - **SensorID**:      IDs of the connected sensors
 - **Sensor data**:   returned data of the connected sensors, **Sensor Error** or **No GPS** is displayed if the sensor is disconnected or returned invalid data

### Electrical Connections 
The following tables show how the sensor and the GPS module are wired to the ESP32.

#### Temperature Sensor
| DHT22 Pin   | ESP32 Pin  |
|:--        |:--        |
|DAT        |15         |
|VCC        |3V3        |
|GND        |GND        |

#### GPS Module
| GPS Pin   | ESP32 Pin |
|:--        |:--        |
|RX         |12         |
|TX         |13         |
|VCC        |3V3        |
|GND        |GND        |

#### ESP32 Pinout

![ESP32 pin out](../media/esp32_pinout.jpg)

[Source: electormagazine.de](https://www.elektormagazine.de/news/auf-einem-board-esp32-oled)



