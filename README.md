# IoT sensor data collection & visualization
**A project developed by Andreas Markwart & Thi Yen Thu Nguyen**

**Module: Mobile & Internet Computing**

**Master programme: Information Engineering and Computer Sciences, Summer semester 2019**

**Rhine-Waal University of Applied Sciences**

**Project coordinator: Mr. Irfan Mirza**

*This document is generated from the original documentation on [GitLab](https://gitlab.com/yenthusiastic/MIC-SS19).*

## 1. Project Description
This project aims to provide a complete open-source hardware & software package for collection and visualization of data from Internet-of-Things (IoT) sensor devices. 

The hardware is a mobile, independent, wireless IoT node using light-weight components and is capable of transmitting sensor data including GPS, temperature and humidity data to an API gateway at a regular interval.

The software, consisting of a database, an API gateway and a web interface application, is developed in Python language and deployed on a remote server as containerized apps and microservices using state-of-the-art Docker framework. The API gateway stores whitelisted sensor data sent by the IoT node in the database while the web application displays the sensor data through a user-friendly interface.

The following figure shows the structure of the project:

![proj_struct](media/project_structure.png)

The result of the web application can be seen at:
**[https://dash.datalog.live](https://dash.datalog.live/dash/)**

Below is a screencap of the web app (as of 02.07.2019):

![screenshot](media/webapp_020719.PNG)

**Explanation of the web interface**
- Each IoT node is marked as an orange circle on the map
- Hovering or clicking on an orange-marked node shows detailed data of the node including:
   - the complete GPS track of the node as a blue path on the map
      - hovering on different points of the track will show the exact latitude and longitude coordinates at that point as well as the timestamp when the data was collected.
   - a table with the last updated GPS location, the temperature, humidity values at that location and the timestamp when data was collected
   - the IDs of the GPS module and the temperature/ humidity sensor belonging to the node
   - a line chart with 2 vertical axes showing the variation of temperature values on the left axis and of humidity values on the right axis.
     - hovering on different points of the line charts will show the exact temperature and humidity values as well as the timestamp when the data was collected.

## 2. Installation Instructions
This section provides step-by-step instructions for deploying the software using Docker on any local or remote machine.
To run the complete software, there are 2 options: build from source code or run pre-built Docker images.

Note: The following instructions are for Linux platforms. However, except for the installtion of Docker & Docker Compose, all docker commands should work the same on a Windows machine with Docker for Windows installed.
To install Docker for Windows (comes with Docker Compose), check the [official guide](https://docs.docker.com/docker-for-windows/install/).
#### Option 1. Run from source code using Dockerfile and Docker Compose
##### Building and running web application
- Install Docker Engine CE
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
audo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt install docker-ce
```
- Install Docker Compose
```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
- Download this Git repository as a zip or clone it:
```bash
git clone https://gitlab.com/yenthusiastic/MIC-SS19/
```
- Go to web app source code directory
```bash
cd webapp_dbaccess
```
- Run Docker compose to build and execute the docker containers for web app and database
```bash
sudo docker-compose build
```
- To run the conainers in detached mode:
```bash
sudo docker-compose up -d
```
The following error might appear when the containers are executed for the first time
```bash
raise CSRFError(reason)
pgadmin_1   | flask_wtf.csrf.CSRFError: 400 Bad Request: The CSRF session token is missing.
```
This is a known issue in Flask that is not yet resolved. Press Ctrl+C to exit, then run the same command again, this time the app should run fine.

The web app will be available at http://0.0.0.0:5000/dash/ and should look like in the screencap from above.

The database can be accessed through the containerized PgAdmin web app at [http://0.0.0.0:5555/](http://0.0.0.0:5555/). For details on how to connect to the postgres databased with PgAdmin, check [Section 3.2 of Software documentation](https://gitlab.com/yenthusiastic/MIC-SS19/blob/master/documentation/software_docs.md#section3.2).


##### Build and run the API Gateway
- Go to API Gateway source code directory
```bash
cd ../receiver
```
- Run Docker to build and execute the docker container from Dockerfile in current folder.
```bash
sudo docker build --tag=api_gateway .
sudo docker run -d -p 5560:5560 api_gateway
```
This containerized API gateway will serve on port 5560.
#### Option 2. Run from Docker images
- Pull and run the modified postgres database image initialized with current sensor data
```bash
sudo docker run -d -p 5432:5432 --name datalog_db yenthusiastic/datalog:db_v0.1
```
- Pull and run the web application on external port 5000 and link to the postgres database named `datalog_db`:
```bash
sudo docker run -d -p 5000:8050 --link datalog_db:postgres yenthusiastic/datalog:webgui_v0.1
```
This command will automatically pull the image to install Python and all dependencies if the image is not already available on the local machine, and run the container after successful installations. The web app will be available at http://0.0.0.0:5000/dash/ and should look like in the screencap from above.

`IMPORTANT NOTE:` If we provide the local host IP address (*127.0.0.1*) at the `port` flag in the above command, i.e. ` -p 127.0.0.1:5000:8050` instead, the app will be accessible at <a href="http://localhost:5000">http://localhost:5000</a>. This means other computers on the same network will not be able to reach it. This is OK for running on a local machine, but for a remote server the localhost IP address cannot be used.

- Pull and run the API gateway image
```bash
sudo docker run -d -p 5560:5560 --name datalog_api yenthusiastic/datalog:api_gateway_v0.1
```
This containerized API gateway will serve on port 5560.
For details on the software development process, check the [Software documentation](https://gitlab.com/yenthusiastic/MIC-SS19/blob/master/documentation/software_docs.md).

#### Test API Gateway
To send dummy data to the database using the API Gateway (when real sensors are not available), go to the **receiver** directory containing API Gateway source code and run the `request_client.py` script
```bash
cd receiver
python3 request_client.py
```
This should return the HTTP OK <200> response code and the new data point should be visible on the web interface.

To build the hardware sensor node for collecting actual data, check the [Hardware documentation](https://gitlab.com/yenthusiastic/MIC-SS19/blob/master/documentation/hardware_docs.md).